var Reloj = require('./reloj');
var segundos = 0;

var reloj = new Reloj();

reloj.on("tick-tock",function(){
    segundos += 1;
    console.log(segundos);
    if(segundos == 5){
        process.exit();
    }
});

reloj.once("tick-tock",function(){
    console.log("Primera vez.");
});
