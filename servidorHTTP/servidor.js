var http = require('http');
var fs = require('fs');
var host = '127.0.0.1';
var puerto = '9000';

var servidor = http.createServer(function(request,response){
    /*Verificar que url ha sido solicitada haya sido la principal*/
    console.log(request.method + " -> " + request.url);
    /*valida si la url solicitada existe*/
    if(request.url == '/'){
        fs.readFile("./index.html",'utf-8',function(err,contenido){
            response.writeHead(200,{'Content-type':'text/html'});
            response.end(contenido);
        });
    }else if(request.url.match(/.css$/)){
        fs.readFile("."+request.url,'utf-8',function(err,contenido){
            response.writeHead(200,{'Content-type':'text/css'});
            response.end(contenido);
        });
    }else if(request.url.match(/.png$/)){
        fs.readFile("."+request.url,function(err,contenido){
            response.writeHead(200,{'Content-type':'image/png'});
            response.end(contenido);
        });
    }
    else{
        response.writeHead(404,{'Content-type':'text/html'});
        response.end("<h1>404 La pagina solicitada no existe.</h1>");
    }
});
servidor.listen(puerto,host,function(){
    console.log("El servidor esta corriendo en: "+host+":"+puerto);
});
