var express = require('express');
var app = express();

/*Indicar la carpeta donde se encuentra el contenido estatico*/
app.use(express.static('public'));
/*Se puede agregar un directorio virual asi*/
//app.use('/virtual',express.static('public'));

app.get('/',function(request,response){
    console.log("Se hizo un llamado GET");
    response.send("Hola desde express");
});

app.listen(3000,function(){
    console.log("Aplicacion corriendo en el puerto 3000");
});
