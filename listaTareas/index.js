var express = require('express');
var bodyParser = require('body-parser');
var session = require('cookie-session');

var app = express();

app.use(session({secret:'nodejs'}));
app.use(bodyParser.urlencoded({extended:false}));
app.set('view engine','ejs');

var tareas = [];

app.get('/',function(request,response){
    response.render('formulario.ejs',{tareas :tareas});
});

app.post('/adicionar',function(request,response){
    var tarea = request.body.nuevaTarea;
    tareas.push(tarea);
    response.redirect('/');
});

app.get('/borrar/:id',function(request,response){
    tareas.splice(request.params.id,1);
    response.redirect('/');
});

app.listen(3000,function(){
    console.log("Corriendo en el puerto 3000");
});
