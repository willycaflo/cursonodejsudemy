var EventEmitter = require("events").EventEmitter;
var util = require("util");

var Reloj = function(){
    var self = this;
    setInterval(function(){
        self.emit("tick-tock");
    },1000);
};

/*La clase reloj hereda la funcionalidad de EventEmitter*/
util.inherits(Reloj,EventEmitter);
module.exports = Reloj;
