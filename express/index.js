var express = require('express');
/*para leer los valores por post esta libreria es util*/
var bodyParser = require('body-parser');
var app = express();

/*Se le indica a express que use body-parser*/
app.use(bodyParser.urlencoded({extended:false}));

app.get('/',function(request,response){
    console.log("Se hizo un llamado GET");
    response.send("Hola desde express");
});

/*Carga el form que se enviara por GET*/
app.get('/form.html',function(request,response){
    response.sendFile(__dirname+"/"+"form.html");
});

/*carga el form que se enviara por POST*/
app.get('/formPost.html',function(request,response){
    response.sendFile(__dirname+"/"+"formPost.html");
});

/*Recoger las variables enviadas por metodo GET*/
app.get('/get',function(request,response){
    var nombre = request.query.nombre;
    var apellido = request.query.apellido;
    response.send("El nombre es "+nombre+"<br/> El apellido es "+apellido);
});

/*Recoger las variables enviadas por metodo POST*/
app.post('/recibirPost',function(request,response){
    var nombre = request.body.nombre;
    var apellido = request.body.apellido;
    response.send("El nombre es "+nombre+"<br/> El apellido es "+apellido);
});


app.listen(3000,function(){
    console.log("Aplicacion corriendo en el puerto 3000");
});
