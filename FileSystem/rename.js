var fs = require('fs');

/*Renombrar un archivo*/
/*fs.rename("archivos/ejempo.txt","archivos/renombrado.txt",function(error){
    if(error){
        throw error;
    }else{
        console.log("El archivo fue renombrado.");
    }
});*/

/*Renombrar un directorio(no importa si no esta vacio)*/
/*fs.rename("test1","dirPrueba",function(error){
    if(error){
        throw error;
    }else{
        console.log("El directorio fue renombrado.");
    }
});*/

/*Mover un archivo*/
/*fs.rename("archivos/renombrado.txt","dirPrueba/renombrado.txt",function(error){
    if(error){
        throw error;
    }else{
        console.log("El archivo fue movido.");
    }
});*/

/*Mover un directorio*/
fs.rename("test2","dirPrueba/test2",function(error){
    if(error){
        throw error;
    }else{
        console.log("El directorio fue movido.");
    }
});
