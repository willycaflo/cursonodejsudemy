var fs = require('fs');
var texto = "Aprende Node JS Facilmente";

fs.writeFile("archivo.txt",texto,function(err){
    if(err){
        throw err;
    }else{
        console.log("El archivo fue creado");
    }
});

var nuevoTexto = "\nCurso por Guillermo Cabrera";

fs.appendFile("archivo.txt",nuevoTexto,function(err){
    if(err){
        throw err;
    }else{
        console.log("Se ha agregado nuevo texto al archivo.");
    }
});
