var fs = require('fs');

/*valida si la carpeta ya existe(verifica si el directorio es accesible)*/
fs.access("carpetaEjemplo",function(err){
    if(err){
        /*Creacion de un directorio*/
        fs.mkdir("carpetaEjemplo",function(err){
            if(err){
                throw err;
            }else{
                console.log("Carpeta creada");
            }
        });
    }else{
        console.log("La carpeta ya existe");
    }
});
