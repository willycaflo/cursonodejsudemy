var fs = require('fs');

/*valida si la carpeta ya existe(verifica si el directorio es accesible)*/
fs.access("Nivel1",function(err){
    if(err){
        /*Creacion de un directorio*/
        fs.mkdir("Nivel1",function(err){
            if(err){
                throw err;
            }else{
                console.log("Carpeta creada");
            }
        });
    }else{
        console.log("La carpeta ya existe");
    }
});

/*valida si la carpeta ya existe(verifica si el directorio es accesible)*/
fs.access("Nivel2",function(err){
    if(err){
        /*Creacion de un directorio*/
        fs.mkdir("Nivel2",function(err){
            if(err){
                throw err;
            }else{
                console.log("Carpeta creada");
            }
        });
    }else{
        console.log("La carpeta ya existe");
    }
});

/*valida si la carpeta ya existe(verifica si el directorio es accesible)*/
fs.access("Nivel3",function(err){
    if(err){
        /*Creacion de un directorio*/
        fs.mkdir("Nivel3",function(err){
            if(err){
                throw err;
            }else{
                console.log("Carpeta creada");
            }
        });
    }else{
        console.log("La carpeta ya existe");
    }
});

/*Leer los archivos de la carpeta temp*/
try{
    var archivos = fs.readdirSync("temp");
}
catch(err){
    throw err;
}

/*Recorrido de los archivos del directorio*/
archivos.forEach(function(nombreArchivo){
    if(nombreArchivo == '.DS_store'){
        df.unlinkSync("temp/"+nombreArchivo);
    }
    if(nombreArchivo.indexOf("_1") !== -1){
        moverArchivo("Nivel1",nombreArchivo);
    }
    if(nombreArchivo.indexOf("_2") !== -1){
        moverArchivo("Nivel2",nombreArchivo);
    }
    if(nombreArchivo.indexOf("_3") !== -1){
        moverArchivo("Nivel3",nombreArchivo);
    }
});

/*Borrar el directorio temp*/
fs.rmdir("temp",function(err){
    if(err){
        throw err;
    }else{
        console.log("La carpeta 'temp' fue eliminada.");
    }
});

/*Funcion para mover un archivo de n directorio a otro*/
function moverArchivo(destino,archivo){
    fs.rename("temp/"+archivo,destino+"/"+archivo,function(err){
        if(err){
            throw err;
        }else {
            console.log("El archivo "+archivo+" se movio a la carpeta "+destino);
        }
    });
}
