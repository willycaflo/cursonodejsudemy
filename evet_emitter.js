var events = require('events');
var EventEmitter = events.EventEmitter;

var ee = new EventEmitter();

ee.once("cansado",function(data){
    console.log("EL jugador numero "+data+" esta cansado por primera vez.");
});

ee.on("cansado",function(data){
    console.log("EL jugador numero "+data+" esta cansado");
});

ee.on("herido",function(data){
    console.log("EL jugador numero "+data+" esta herido");
});

ee.emit("cansado","10");
ee.emit("cansado","10");
/*Evita que se siga ejecutando el listener*/
ee.removeAllListeners("cansado");
ee.emit("cansado","10");
ee.emit("herido","10");
